﻿using AutoMapper;
using Model.BaseModel.System;
using Repository.Impl;
using Service.Impl;

namespace Service
{
    public class SysUsersService : BaseService<SysUsers>, ISysUsersService
    {
        private readonly IBaseRepository<SysUsers> _dal;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public SysUsersService(IBaseRepository<SysUsers> dal, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._dal = dal;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            base.baseDal = dal;
        }

    }
}