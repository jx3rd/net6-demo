﻿using SqlSugar;
using System.Linq.Expressions;

namespace Service.Impl
{
    public interface IBaseService<TEntity> where TEntity : class
    {


        #region Async

        Task<bool> Exists(Expression<Func<TEntity, bool>> where = null);
        Task<int> QueryCount(Expression<Func<TEntity, bool>> where = null);


        Task<int> Add(TEntity model);
        Task<bool> AddList(List<TEntity> entities);
        Task<bool> AddOrUpdate(TEntity entities, string UpIgnore = "AddTime");
        Task<bool> AddOrUpdateList(List<TEntity> list, string UpIgnore = "AddTime");



        Task<bool> DeleteById(object id);
        Task<bool> DeleteByIdLogic(object id, string logicKey = "IsDelete");
        Task<bool> Delete(TEntity model);
        Task<bool> DeleteLogic(TEntity model, string logicKey = "IsDelete");
        Task<bool> DeleteByIds(object[] ids);
        Task<bool> DeleteByIdsLogic(object[] ids, string logicKey = "IsDelete");


        Task<bool> Update(TEntity model);
        Task<bool> Update(TEntity entity, string strWhere);
        Task<bool> UpdateList(List<TEntity> entities, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "");
        Task<bool> Update(TEntity entity, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "");


        Task<TEntity> QueryByID(object objId, Expression<Func<TEntity, bool>> whereExpression = null);
        Task<List<TEntity>> QueryByIDs(object[] lstIds, Expression<Func<TEntity, bool>> whereExpression = null);
        Task<List<TEntity>> QueryAll();
        Task<List<TEntity>> Query(string strWhere);
        Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression);
        Task<TEntity> QuerySingle(Expression<Func<TEntity, bool>> whereExpression);
        Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression, Expression<Func<TEntity, object>> orderByExpression, bool isAsc = true);
        Task<List<TEntity>> Query(string strWhere, string strOrderByFileds);
        Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression, int intTop, string strOrderByFileds);
        Task<List<TEntity>> QueryPage(string sql, SugarParameter sugarParameter, RefAsync<int> TotalCount, int intPageIndex = 1, int intPageSize = 20, string strOrderByFileds = null);
        Task<List<TEntity>> QueryPage(Expression<Func<TEntity, bool>> whereExpression, RefAsync<int> TotalCount, int intPageIndex, int intPageSize = 20, string strOrderByFileds = null);
        Task<List<TEntity>> SqlQuery(string sql);
        Task<TEntity> QueryFirst(Expression<Func<TEntity, bool>> whereExpression);

        #endregion


        #region Sync

        bool ExistsSync(Expression<Func<TEntity, bool>> where = null);
        int QueryCountSync(Expression<Func<TEntity, bool>> where = null);
        int AddSync(TEntity model);
        bool AddListSync(List<TEntity> entities);
        bool AddOrUpdateSync(TEntity entities, string UpIgnore = "AddTime");
        bool AddOrUpdateListSync(List<TEntity> entities, string UpIgnore = "AddTime");



        bool DeleteByIdSync(object id);
        bool DeleteByIdLogicSync(object id, string logicKey = "IsDelete");
        bool DeleteSync(TEntity model);
        bool DeleteLogicSync(TEntity model, string logicKey = "IsDelete");
        bool DeleteByIdsSync(object[] ids);
        bool DeleteByIdsLogicSync(object[] ids, string logicKey = "IsDelete");



        bool UpdateSync(TEntity model);
        bool UpdateListSync(List<TEntity> entities, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "");
        bool UpdateSync(TEntity entity, string strWhere);
        bool UpdateSync(TEntity entity, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "");


        List<TEntity> QueryAllSync();
        TEntity QueryByIDSync(object objId, Expression<Func<TEntity, bool>> whereExpression = null);
        List<TEntity> QueryByIDsSync(object[] lstIds, Expression<Func<TEntity, bool>> whereExpression = null);
        List<TEntity> QuerySync(string strWhere);
        List<TEntity> QuerySync(Expression<Func<TEntity, bool>> whereExpression);
        TEntity QuerySingleSync(Expression<Func<TEntity, bool>> whereExpression);
        List<TEntity> QuerySync(Expression<Func<TEntity, bool>> whereExpression, Expression<Func<TEntity, object>> orderByExpression, bool isAsc = true);
        List<TEntity> QuerySync(string strWhere, string strOrderByFileds);
        List<TEntity> QuerySync(Expression<Func<TEntity, bool>> whereExpression, int intTop, string strOrderByFileds);
        List<TEntity> QueryPageSync(string sql, SugarParameter sugarParameter, int TotalCount, int intPageIndex, int intPageSize = 20, string strOrderByFileds = null);
        List<TEntity> QueryPageSync(Expression<Func<TEntity, bool>> whereExpression, ref int TotalCount, int intPageIndex = 1, int intPageSize = 20, string strOrderByFileds = null);
        List<TEntity> SqlQuerySync(string sql);
        TEntity QueryFirstSync(Expression<Func<TEntity, bool>> whereExpression);
        TEntity SqlQuerySingleSync(string sql);
        #endregion


    }
}
