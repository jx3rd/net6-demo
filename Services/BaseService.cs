﻿using Repository.Impl;
using Service.Impl;
using SqlSugar;
using System.Linq.Expressions;

namespace Service
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class, new()
    {
        public IBaseRepository<TEntity> baseDal;


        #region Async

        #region Other
        /// <summary>
        /// 判断是否存在记录
        /// </summary>
        /// <param name="where">实体类</param>
        /// <returns></returns>
        public async Task<bool> Exists(Expression<Func<TEntity, bool>> where = null)
        {
            return await baseDal.Exists(where);
        }

        /// <summary>
        /// 统计数据数量
        /// </summary>
        /// <param name="where">实体类</param>
        /// <returns></returns>
        public async Task<int> QueryCount(Expression<Func<TEntity, bool>> where = null)
        {
            return await baseDal.QueryCount(where);
        }
        #endregion

        #region Add
        /// <summary>
        /// 写入实体数据
        /// </summary>
        /// <param name="entity">实体类集合</param>
        /// <returns></returns>
        public async Task<int> Add(TEntity entity)
        {
            return await baseDal.Add(entity);
        }

        /// <summary>
        /// 批量写入实体数据
        /// </summary>
        /// <param name="entities">实体类集合</param>
        /// <returns></returns>
        public async Task<bool> AddList(List<TEntity> entities)
        {
            return await baseDal.AddList(entities);
        }

        /// <summary>
        /// 插入或更新
        /// </summary>
        /// <param name="entities">实体类集合</param>
        /// <returns></returns>
        public async Task<bool> AddOrUpdate(TEntity entities, string UpIgnore = "AddTime")
        {
            return await baseDal.AddOrUpdate(entities, UpIgnore);
        }

        /// <summary>
        /// 插入或更新列表
        /// </summary>
        /// <param name="entities">实体类集合</param>
        /// <returns></returns>
        public async Task<bool> AddOrUpdateList(List<TEntity> entities, string UpIgnore = "AddTime")
        {
            return await baseDal.AddOrUpdateList(entities, UpIgnore);
        }
        #endregion

        #region Delete

        /// <summary>
        /// 根据实体删除一条数据
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        public async Task<bool> Delete(TEntity entity)
        {
            return await baseDal.Delete(entity);
        }

        /// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        public async Task<bool> DeleteLogic(TEntity entity, string logicKey = "IsDelete")
        {
            return await baseDal.DeleteLogic(entity, logicKey);
        }

        /// <summary>
        /// 删除指定ID的数据
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        public async Task<bool> DeleteById(object id)
        {
            return await baseDal.DeleteById(id);
        }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id">主键ID集合</param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdLogic(object id, string logicKey = "IsDelete")
        {
            return await baseDal.DeleteByIdLogic(id, logicKey);
        }

        /// <summary>
        /// 删除指定ID集合的数据(批量删除)
        /// </summary>
        /// <param name="ids">主键ID集合</param>
        /// <returns></returns>
        public async Task<bool> DeleteByIds(object[] ids)
        {
            return await baseDal.DeleteByIds(ids);
        }

        /// <summary>
        /// 批量逻辑删除
        /// </summary>
        /// <param name="ids">主键ID集合</param>
        /// <returns></returns>
        public async Task<bool> DeleteByIdsLogic(object[] ids, string logicKey = "IsDelete")
        {
            return await baseDal.DeleteByIdsLogic(ids, logicKey);
        }
        #endregion

        #region Update
        /// <summary>
        /// 更新实体数据
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        public async Task<bool> Update(TEntity entity)
        {
            return await baseDal.Update(entity);
        }

        /// <summary>
        /// 批量更新实体数据
        /// </summary>
        /// <param name="entities">实体类集合</param>
        /// <returns></returns>
        public async Task<bool> UpdateList(List<TEntity> entities, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "")
        {
            return await baseDal.UpdateList(entities, lstColumns, lstIgnoreColumns, strWhere);
        }

        public async Task<bool> Update(TEntity entity, string strWhere)
        {
            return await baseDal.Update(entity, strWhere);
        }

        public async Task<bool> Update(TEntity entity, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "")
        {
            return await baseDal.Update(entity, lstColumns, lstIgnoreColumns, strWhere);
        }

        #endregion

        #region Query

        /// <summary>
        /// 根据ID查询一条数据        
        /// </summary>
        /// <param name="objId">id（必须指定主键特性 [SugarColumn(IsPrimaryKey=true)]），如果是联合主键，请使用Where条件</param>
        /// <param name="whereExpression"></param>
        public async Task<TEntity> QueryByID(object objId, Expression<Func<TEntity, bool>> whereExpression = null)
        {
            return await baseDal.QueryByID(objId, whereExpression);
        }

        /// <summary>
        /// 根据ID查询数据        
        /// </summary>
        /// <param name="lstIds">id列表（必须指定主键特性 [SugarColumn(IsPrimaryKey=true)]），如果是联合主键，请使用Where条件</param>
        /// <returns>数据实体列表</returns>
        public async Task<List<TEntity>> QueryByIDs(object[] lstIds, Expression<Func<TEntity, bool>> whereExpression = null)
        {
            return await baseDal.QueryByIDs(lstIds, whereExpression);
        }

        /// <summary>
        /// 查询所有数据（异步）                
        /// </summary>
        /// <returns>数据列表</returns>
        public async Task<List<TEntity>> QueryAll()
        {
            return await baseDal.QueryAll();
        }

        /// <summary>
        /// 查询数据列表        
        /// </summary>
        /// <param name="strWhere">条件</param>
        /// <returns>数据列表</returns>
        public async Task<List<TEntity>> Query(string strWhere)
        {
            return await baseDal.Query(strWhere);
        }

        /// <summary>
        /// 查询数据列表（异步）              
        /// </summary>
        /// <param name="whereExpression">whereExpression</param>
        /// <returns>数据列表</returns>
        public async Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression)
        {
            return await baseDal.Query(whereExpression);
        }

        /// <summary>
        /// 查询数据列表（异步）              
        /// </summary>
        /// <param name="whereExpression">whereExpression</param>
        /// <returns>数据列表</returns>
        public async Task<TEntity> QuerySingle(Expression<Func<TEntity, bool>> whereExpression)
        {
            return await baseDal.QuerySingle(whereExpression);
        }


        /// <summary>
        /// 查询一个列表        
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public async Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression, Expression<Func<TEntity, object>> orderByExpression, bool isAsc = true)
        {
            return await baseDal.Query(whereExpression, orderByExpression, isAsc);
        }

        /// <summary>
        /// 查询一个列表        
        /// </summary>
        /// <param name="strWhere">条件</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public async Task<List<TEntity>> Query(string strWhere, string strOrderByFileds)
        {
            return await baseDal.Query(strWhere, strOrderByFileds);
        }

        /// <summary>
        /// 查询前N条数据        
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="intTop">前N条</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public async Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression, int intTop, string strOrderByFileds)
        {
            return await baseDal.Query(whereExpression, intTop, strOrderByFileds);
        }

        /// <summary>
        /// sql分页查询
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="sugarParameter">  new SugarParameter("@ids", new int[] { 1, 2 })} </param>
        /// <param name="TotalCount"></param>
        /// <param name="intPageIndex"></param>
        /// <param name="intPageSize"></param>
        /// <param name="strOrderByFileds"></param>
        /// <returns></returns>
        public async Task<List<TEntity>> QueryPage(string sql, SugarParameter sugarParameter, RefAsync<int> TotalCount, int intPageIndex = 1, int intPageSize = 20, string strOrderByFileds = null)
        {
            return await baseDal.QueryPage(sql, sugarParameter, TotalCount, intPageIndex, intPageSize, strOrderByFileds);
        }
        /// <summary>
        /// 分页查询（异步）        
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="intPageIndex">页码（1开始）</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="TotalCount">数据总量</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public async Task<List<TEntity>> QueryPage(Expression<Func<TEntity, bool>> whereExpression, RefAsync<int> TotalCount, int intPageIndex = 1, int intPageSize = 20, string strOrderByFileds = null)
        {
            return await baseDal.QueryPage(whereExpression, TotalCount, intPageIndex, intPageSize, strOrderByFileds);
        }
        /// <summary>
        /// 查询返回实体集合（异步） 
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <returns></returns>
        public async Task<List<TEntity>> SqlQuery(string sql)
        {
            return await baseDal.SqlQuery(sql);
        }
        /// <summary>
        /// 查询返回实体集合（异步） 
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<TEntity> QueryFirst(Expression<Func<TEntity, bool>> whereExpression)
        {
            return await baseDal.QueryFirst(whereExpression);
        }
        #endregion

        #endregion


        #region Sync

        #region Other
        /// <summary>
        /// 判断是否存在记录(同步)
        /// </summary>
        /// <param name="where">实体类</param>
        /// <returns></returns>
        public bool ExistsSync(Expression<Func<TEntity, bool>> where = null)
        {
            return baseDal.ExistsSync();
        }

        /// <summary>
        /// 统计数据数量（同步）
        /// </summary>
        /// <param name="where">实体类</param>
        /// <returns></returns>
        public int QueryCountSync(Expression<Func<TEntity, bool>> where = null)
        {
            return baseDal.QueryCountSync();
        }

        #endregion

        #region Add
        /// <summary>
        /// 写入实体数据（同步）
        /// </summary>
        /// <param name="entity">实体类</param>
        /// <returns></returns>
        public int AddSync(TEntity entity)
        {
            return baseDal.AddSync(entity);
        }

        /// <summary>
        /// 批量写入实体数据（同步）
        /// </summary>
        /// <param name="entities">实体类集合</param>
        /// <returns></returns>
        public bool AddListSync(List<TEntity> entities)
        {
            return baseDal.AddListSync(entities);
        }

        /// <summary>
        /// 插入或更新（同步）
        /// </summary>
        /// <param name="entities">实体类集合</param>
        /// <returns></returns>
        public bool AddOrUpdateSync(TEntity entities, string UpIgnore = "AddTime")
        {
            return baseDal.AddOrUpdateSync(entities, UpIgnore);
        }

        /// <summary>
        /// 插入或更新列表
        /// </summary>
        /// <param name="entities">实体类集合</param>
        /// <returns></returns>
        public bool AddOrUpdateListSync(List<TEntity> entities, string UpIgnore = "AddTime")
        {
            return baseDal.AddOrUpdateListSync(entities, UpIgnore);
        }
        #endregion

        #region Delete
        public bool DeleteSync(TEntity entity)
        {
            return baseDal.DeleteSync(entity);
        }
        public bool DeleteLogicSync(TEntity entity, string logicKey = "IsDelete")
        {
            return baseDal.DeleteLogicSync(entity, logicKey);
        }
        public bool DeleteByIdSync(object id)
        {
            return baseDal.DeleteByIdSync(id);
        }
        public bool DeleteByIdLogicSync(object id, string logicKey = "IsDelete")
        {
            return baseDal.DeleteByIdLogicSync(id, logicKey);
        }
        public bool DeleteByIdsSync(object[] ids)
        {
            return baseDal.DeleteByIdsSync(ids);
        }
        public bool DeleteByIdsLogicSync(object[] ids, string logicKey = "IsDelete")
        {
            return baseDal.DeleteByIdsLogicSync(ids, logicKey);
        }
        #endregion

        #region Update
        public bool UpdateSync(TEntity entity)
        {
            return baseDal.UpdateSync(entity);
        }
        public bool UpdateListSync(List<TEntity> entities, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "")
        {
            return baseDal.UpdateListSync(entities, lstColumns, lstIgnoreColumns, strWhere);
        }
        public bool UpdateSync(TEntity entity, string strWhere)
        {
            return baseDal.UpdateSync(entity, strWhere);
        }
        public bool UpdateSync(TEntity entity, List<string> lstColumns = null, List<string> lstIgnoreColumns = null, string strWhere = "")
        {
            return baseDal.UpdateSync(entity, lstColumns, lstIgnoreColumns, strWhere);
        }
        #endregion

        #region Query
        /// <summary>
        /// 根据ID查询单条数据（同步）    
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public TEntity QueryByIDSync(object objId, Expression<Func<TEntity, bool>> whereExpression = null)
        {
            return baseDal.QueryByIDSync(objId, whereExpression);
        }

        /// <summary>
        /// 根据ID查询数据（同步）          
        /// </summary>
        /// <param name="lstIds">id列表（必须指定主键特性 [SugarColumn(IsPrimaryKey=true)]），如果是联合主键，请使用Where条件</param>
        /// <returns>数据实体列表</returns>
        public List<TEntity> QueryByIDsSync(object[] lstIds, Expression<Func<TEntity, bool>> whereExpression = null)
        {
            return baseDal.QueryByIDsSync(lstIds, whereExpression);
        }

        /// <summary>
        /// 查询所有数据（同步）        
        /// </summary>
        /// <returns>数据列表</returns>
        public List<TEntity> QueryAllSync()
        {
            return baseDal.QueryAllSync();
        }

        /// <summary>
        /// 查询数据列表（同步）         
        /// </summary>
        /// <param name="strWhere">条件</param>
        /// <returns>数据列表</returns>
        public List<TEntity> QuerySync(string strWhere)
        {
            return baseDal.QuerySync(strWhere);
        }

        /// <summary>
        /// 查询数据列表（同步）         
        /// </summary>
        /// <param name="whereExpression">whereExpression</param>
        /// <returns>数据列表</returns>
        public List<TEntity> QuerySync(Expression<Func<TEntity, bool>> whereExpression)
        {
            return baseDal.QuerySync(whereExpression);
        }

        /// <summary>
        /// 查询数据列表（同步）
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public TEntity QuerySingleSync(Expression<Func<TEntity, bool>> whereExpression)
        {
            return baseDal.QuerySingleSync(whereExpression);

        }

        /// <summary>
        /// 查询一个列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        public List<TEntity> QuerySync(Expression<Func<TEntity, bool>> whereExpression, Expression<Func<TEntity, object>> orderByExpression, bool isAsc = true)
        {
            return baseDal.QuerySync(whereExpression, orderByExpression, isAsc);
        }

        /// <summary>
        /// 查询一个列表        
        /// </summary>
        /// <param name="strWhere">条件</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public List<TEntity> QuerySync(string strWhere, string strOrderByFileds)
        {
            return baseDal.QuerySync(strWhere, strOrderByFileds);
        }

        /// <summary>
        /// 查询前N条数据        
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="intTop">前N条</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public List<TEntity> QuerySync(Expression<Func<TEntity, bool>> whereExpression, int intTop, string strOrderByFileds)
        {
            return baseDal.QuerySync(whereExpression, intTop, strOrderByFileds);
        }

        /// <summary>
        /// 分页查询sql       
        /// </summary>
        /// <param name="sql">条件表达式</param>
        /// <param name="intPageIndex">页码（1开始）</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="TotalCount">数据总量</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public List<TEntity> QueryPageSync(string sql, SugarParameter sugarParameter, int TotalCount, int intPageIndex = 1, int intPageSize = 20, string strOrderByFileds = null)
        {
            return baseDal.QueryPageSync(sql, sugarParameter, TotalCount, intPageIndex, intPageSize, strOrderByFileds);
        }

        /// <summary>
        /// 分页查询（同步）        
        /// </summary>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="intPageIndex">页码（1开始）</param>
        /// <param name="intPageSize">页大小</param>
        /// <param name="TotalCount">数据总量</param>
        /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
        /// <returns>数据列表</returns>
        public List<TEntity> QueryPageSync(Expression<Func<TEntity, bool>> whereExpression, ref int TotalCount, int intPageIndex = 1, int intPageSize = 20, string strOrderByFileds = null)
        {
            return baseDal.QueryPageSync(whereExpression, ref TotalCount, intPageIndex, intPageSize, strOrderByFileds);

        }

        /// <summary>
        /// 查询返回实体集合（同步）    
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <returns></returns>
        public List<TEntity> SqlQuerySync(string sql)
        {
            return baseDal.SqlQuerySync(sql);
        }

        /// <summary>
        /// 表达式查询第一条记录
        /// </summary>
        /// <param name="whereExpression">表达式</param>
        /// <returns></returns>
        public TEntity QueryFirstSync(Expression<Func<TEntity, bool>> whereExpression)
        {
            return baseDal.QueryFirstSync(whereExpression);
        }

        /// <summary>
        /// 查询第一条记录
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <returns></returns>
        public TEntity SqlQuerySingleSync(string sql)
        {
            return baseDal.SqlQuerySingleSync(sql);
        }
        #endregion

        #endregion


    }
}
