﻿using Lycoris.RabbitMQ.Extensions;
using Lycoris.RabbitMQ.Extensions.DataModel;

namespace Services.RabbitMqServices.Consumer
{
    public class ArticleSearchConsumer : BaseRabbitConsumerListener
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        protected override Task<ReceivedHandler> ReceivedAsync(string body)
        {
            Console.WriteLine($"消费者 ==> 消费消息：{body}");
            return Task.FromResult(ReceivedHandler.Commit);
        }
    }
}
