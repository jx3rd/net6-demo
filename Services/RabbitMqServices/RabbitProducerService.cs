﻿using CareerUp.Services.RabbitMqServices.Impl;
using Lycoris.RabbitMQ.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareerUp.Services.RabbitMqServices
{
    public class RabbitProducerService : BaseRabbitProducerService, IRabbitProducerService
    {
        public RabbitProducerService(IRabbitProducerFactory rabbitProducerFactory) : base(rabbitProducerFactory)
        {
        }
        /// <summary>
        /// 生产者方法
        /// </summary>
        public void Test()
        {
            Producer.Publish("route1", "生产者生产的消息TestConsumer！！！！！！！！！！");
        }
    }
}
