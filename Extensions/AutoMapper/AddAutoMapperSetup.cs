﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Extensions.AutoMapper
{
    public static class AddAutoMapperSetup
    {
        public static void AutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(AutoMapperConfig));
            AutoMapperConfig.RegisterMappings();
        }
    }
}
