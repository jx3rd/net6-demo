﻿using AutoMapper;
using Model.BaseModel.System;
using Model.Dto.System;

namespace Extensions.AutoMapper
{
    public class CustomProfile : Profile
    {
        public CustomProfile()
        {
            //CreateMap<SystemOrgUser, SystemUsers>()
            //    .ForMember(b => b.UserCode, cf => cf.MapFrom(a => a.DelFlag))
            //    .ForMember(b => b.CellPhone, cf => cf.MapFrom(a => a.UserCode));

            CreateMap<SysUsersDto, SysUsers>();
        }
    }
}
