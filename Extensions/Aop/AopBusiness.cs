﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using Model.CustomAttribute;

namespace Extensions.Aop
{
    public class AopBusiness : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            var method = invocation.MethodInvocationTarget ?? invocation.Method;
            //对当前方法的特性验证
            //如果需要验证
            var ServceAttribute = method.GetCustomAttributes(true).FirstOrDefault(x => x.GetType() == typeof(ServceAttribute));

            invocation.Proceed();

            if (ServceAttribute is ServceAttribute qCachingAttribute)
            {
                Console.WriteLine("方法执行后调用");
            }


        }
    }
}
