﻿using Microsoft.Extensions.DependencyInjection;
using Model;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Helper;

namespace Extensions.Extensions
{
    public static class DefaultTable
    {
        public static void CreateDefaultTable(this IServiceCollection services)
        {
            SqlSugarClient db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = AppHelper.GetConnection("Service"),
                DbType = DbType.SqlServer,
                IsAutoCloseConnection = true
            });

            if (db.Ado.IsValidConnection()) {

                // 创建数据库
                db.DbMaintenance.CreateDatabase();

                List<Type> types = new List<Type>();    
                //types.Add(typeof(SystemUsers));  

                // 创建表
                db.CodeFirst.InitTables(types.ToArray());
 
            }

        }
    }
}
