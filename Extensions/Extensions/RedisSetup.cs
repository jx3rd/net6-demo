﻿using Autofac.Core;
using Microsoft.Extensions.DependencyInjection;
using System;
using Utils.Helper;
using Utils.Redis;
using Wei.RedisHelper;

namespace Extensions.Extensions
{
    public static class RedisSetup
    {
        /// <summary>
        /// 在构造函数里面注入RedisHelper使用
        /// </summary>
        /// <param name="service"></param>
        public static IServiceCollection AddRedisSetUp(this IServiceCollection services)
        {
            RedisOption redisOption = new RedisOption();
            redisOption.RedisConnectionString = AppHelper.app(new string[] { "RedisConnection" });//获取连接字符串
            services.AddSingleton(redisOption);
            ///Wei.RedisHelper NuGet包中封装的Redis方法。
            services.AddSingleton(typeof(RedisClient));
            ///工具类中继承重写RedisClient中的方法，后续添加注释方便调用查看使用。
            services.AddSingleton(typeof(RedisHelper));
            return services;
        }

    }
}
