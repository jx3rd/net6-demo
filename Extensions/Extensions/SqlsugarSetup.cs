﻿using Microsoft.Extensions.DependencyInjection;
using SqlSugar;
using System;
using Utils.Helper;

namespace Extensions.Extensions
{
    /// <summary>
    /// 静态SqlSugar扩展类
    /// </summary>
    public static class SqlsugarSetup
    {
        /// <summary>
        /// sqlsugar
        /// </summary>
        /// <param name="service">服务</param>
        /// <param name="configuration">配置文件</param>
        public static void AppSqlsugarSetup(this IServiceCollection service)
        {
            if (service == null) throw new ArgumentNullException(nameof(service));

            service.AddScoped<ISqlSugarClient>(o =>
            {
                return new SqlSugarScope(new ConnectionConfig()
                {
                    ConnectionString = AppHelper.app("ConnectionStrings:SqlServer"),//必填, 数据库连接字符串
                    DbType = SqlSugar.DbType.SqlServer,//必填, 数据库类型
                    IsAutoCloseConnection = true,//默认false, 时候知道关闭数据库连接, 设置为true无需使用using或者Close操作
                    InitKeyType = InitKeyType.SystemTable//默认SystemTable, 字段信息读取, 如：该属性是不是主键，标识列等等信息
                },
                db =>
                {
                    db.Aop.OnLogExecuting = (sql, pars) =>
                    {
                        Console.WriteLine(sql);
                        Console.WriteLine("--------------------------------");
                    };
                });
            });
        }
    }
}
