﻿using Model;
using SqlSugar;

namespace Repository.Impl
{
    public interface IUnitOfWork
    {
        public ISqlSugarClient db { get; }

        SqlSugarScope GetDbClient();
        // 开始事务
        void BeginTran();
        // 提交事务
        void CommitTran();
        // 回滚事务
        void RollbackTran();

        public List<IConditionalModel> GetQueryParam(List<QueryParam> str);
    }
}
