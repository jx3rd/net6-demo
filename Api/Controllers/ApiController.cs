﻿using Model.ApiModel.ApiResult;
using Microsoft.AspNetCore.Mvc;


namespace CareerPower.Api.Controllers
{
    [ApiController]
    public abstract class ApiController : ControllerBase
    {
        /// <summary>
        /// 成功返回结果
        /// </summary>
        /// <param name="message">消息</param>
        /// <returns></returns>
        protected ResponseResult SuccessResult(string message)
        {
            return ResponseResult.SuccessResult(message);
        }

        /// <summary>
        /// 返回结果
        /// </summary>
        /// <param name="success"></param>
        /// <param name="message">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected ResponseResult Result(bool success, object data, string message = "")
        {
            return ResponseResult.Result(success, data, message);
        }

        /// <summary>
        /// 失败返回结果
        /// </summary>
        /// <param name="message">失败信息</param>
        /// <returns></returns>
        protected ResponseResult FailResult(string message)
        {
            return ResponseResult.FailResult(message);
        }

        /// <summary>
        /// 失败返回结果
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected ResponseResult<T> FailResult<T>(string message, T data)
        {
            return ResponseResult<T>.FailResult(message, data);
        }
    }
}
