using Api.Filter;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Extensions.AutoMapper;
using Extensions.Extensions;
using FluentValidation.AspNetCore;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Model.ApiModel.ApiGroup;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Text;
using Utils.Helper;
using Model.ValidationModel.System;
using Model.Dto.System;

var builder = WebApplication.CreateBuilder(args);
var basePath = AppContext.BaseDirectory;

//builder.WebHost.UseUrls("http://*:5000");

#region 注入数据库
builder.Services.AppSqlsugarSetup();

//builder.Services.AddDbContext<BDContext>(option =>
//{
//    string DbConnectionString = AppHelper.GetConnection("Service");
//    option.UseSqlServer(DbConnectionString);
//});

//builder.Services.CreateDefaultTable();
#endregion

#region 注入Redis
//builder.Services.AddRedisSetUp();
#endregion

#region 注入Mq
builder.Services.AddRabbitMqSetUp();
#endregion

#region 配置AutoMapper
builder.Services.AutoMapperSetup();
#endregion

#region Autofac，批量注入Service、Repository
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(builder =>
{
    builder.RegisterModule(new AutofacModuleRegister());
});
#endregion

#region 添加MemoryChache缓存配置
builder.Services.AddMemoryCacheSetup();
#endregion

#region 注入http上下文
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#endregion

#region 注册全局过滤器
builder.Services.AddControllers(builder =>
{
    // 全局添加异常过滤器
    builder.Filters.Add(typeof(GlobalExceptionsFilter));
    // 全局添加自定义模型验证过滤器
    //builder.Filters.Add<DataValidationFilter>();
});

//builder.Services.AddFluentValidationAutoValidation();
//builder.Services.AddScoped<IValidator<SysUsersDto>, SysUsersValidator>();
#endregion

#region json格式格式化
builder.Services.AddControllers().AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
    //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
    options.SerializerSettings.Converters.Add(new StringEnumConverter());
});
#endregion

#region 注册log4net日志
//builder.Host.ConfigureLogging((hostingContext, builder) =>
//{
//    builder.AddFilter("System", LogLevel.Error); //过滤掉系统默认的一些日志
//    builder.AddFilter("Microsoft", LogLevel.Error);//过滤掉系统默认的一些日志
//    //builder.SetMinimumLevel(LogLevel.Error);
//    builder.AddLog4Net(Path.Combine(Directory.GetCurrentDirectory(), "Log/Log4net.config"));
//});
#endregion

#region Swagger
builder.Services.AddSwaggerSetUp();
#endregion

#region JWT验证
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = true,//是否验证Issuer
        ValidateAudience = true, //是否验证Audience
        ValidateLifetime = true,//是否验证失效时间
        ValidIssuer = AppHelper.GetJsonValue("JWT:Issuer"),
        ValidAudience = AppHelper.GetJsonValue("JWT:Audience"),
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppHelper.GetJsonValue("JWT:SecurityKey")))
    };
});
#endregion

#region 跨域处理允许所有来源
builder.Services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
{
    builder.AllowAnyMethod()
    .SetIsOriginAllowed(_ => true)
    .AllowAnyHeader()
    .AllowCredentials();
}));

#endregion


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();

    #region SwaggerUI
    app.UseSwaggerUI(options =>
    {
        //遍历ApiGroupNames所有枚举值生成接口文档，Skip(1)是因为Enum第一个FieldInfo是内置的一个Int值
        typeof(ApiGroupNames).GetFields().Skip(1).ToList().ForEach(f =>
        {
            //获取枚举值上的特性
            var info = f.GetCustomAttributes(typeof(GroupInfoAttribute), false).OfType<GroupInfoAttribute>().FirstOrDefault();
            options.SwaggerEndpoint($"/swagger/{f.Name}/swagger.json", info != null ? info.Title : f.Name);

        });
        options.SwaggerEndpoint("/swagger/NoGroup/swagger.json", "Others");
    });
    #endregion
}

app.UseCors("CorsPolicy");

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

