﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.Enums
{
    public class EncodeUtils
    {
        /// <summary>
        /// 将对象转成字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Encode(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// 将字符串转成对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T Decode<T>(string json) where T : class
        {
            JsonSerializer serializer = new JsonSerializer();
            StringReader sr = new StringReader(json);
            object o = serializer.Deserialize(new JsonTextReader(sr), typeof(T));
            T t = o as T;
            return t;
        }
    }
}
