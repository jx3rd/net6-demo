﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wei.RedisHelper;

namespace Utils.Redis
{
    public class RedisHelper : RedisClient
    {
        public RedisHelper(RedisOption option) : base(option)
        {
            ///构造函数中直接创建Redis数据库连接
            GetDatabase(1, true);
        }

        public new void GetDatabase(int dbNum = -1, object asyncState = null)
        {
            base.GetDatabase(dbNum, asyncState);
        }

        public new bool StringSet(string key, string value, TimeSpan? expiry = null)
        {
            return base.StringSet(key, value, expiry);
        }

        public new Task<bool> StringSetAsync(string key, string value, TimeSpan? expiry = null)
        {
            return base.StringSetAsync(key, value, expiry);
        }

        public new bool StringSet(List<KeyValuePair<RedisKey, RedisValue>> keyValues)
        {
            return base.StringSet(keyValues);
        }

        public new Task<bool> StringSetAsync(List<KeyValuePair<RedisKey, RedisValue>> keyValues)
        {
            return base.StringSetAsync(keyValues);
        }

        public new bool StringSet<T>(string key, T obj, TimeSpan? expiry = null)
        {
            return base.StringSet(key, obj, expiry);
        }

        public new Task<bool> StringSetAsync<T>(string key, T obj, TimeSpan? expiry = null)
        {
            return base.StringSetAsync(key, obj, expiry);
        }



        public new string? StringGet(string key)
        {
            return (string?)base.StringGet(key);
        }

        public new async Task<string?> StringGetAsync(string key)
        {
            return (string?)(await base.StringGetAsync(key));
        }

        public new RedisValue[] StringGet(List<string> listKey)
        {
            return base.StringGet(listKey);
        }

        public new Task<RedisValue[]> StringGetAsync(List<string> listKey)
        {
            return base.StringGetAsync(listKey);
        }

        public new T StringGet<T>(string key)
        {
            return base.StringGet<T>(key);
        }

        public new async Task<T> StringGetAsync<T>(string key)
        {
            return await base.StringGetAsync<T>(key);
        }



        public new long StringIncrement(string key, long val = 1L)
        {
            return base.StringIncrement(key, val);
        }

        public new Task<long> StringIncrementAsync(string key, long val = 1L)
        {
            return base.StringIncrementAsync(key, val);
        }

        public new long StringDecrement(string key, long val = 1L)
        {
            return base.StringDecrement(key, val);
        }

        public new Task<long> StringDecrementAsync(string key, long val = 1L)
        {
            return base.StringDecrementAsync(key, val);
        }

        public new bool HashExists(string key, string dataKey)
        {
            return base.HashExists(key, dataKey);
        }

        public new Task<bool> HashExistsAsync(string key, string dataKey)
        {
            return base.HashExistsAsync(key, dataKey);
        }

        public new bool HashSet<T>(string key, string dataKey, T t)
        {
            return base.HashSet(key, dataKey, t);
        }

        public new Task<bool> HashSetAsync<T>(string key, string dataKey, T t)
        {
            return base.HashSetAsync(key, dataKey, t);
        }

        public new bool HashDelete(string key, string dataKey)
        {
            return base.HashDelete(key, dataKey);
        }

        public new Task<bool> HashDeleteAsync(string key, string dataKey)
        {
            return base.HashDeleteAsync(key, dataKey);
        }

        public new long HashDelete(string key, List<RedisValue> dataKeys)
        {
            return base.HashDelete(key, dataKeys);
        }

        public new Task<long> HashDeleteAsync(string key, List<RedisValue> dataKeys)
        {
            return base.HashDeleteAsync(key, dataKeys);
        }

        public new T HashGet<T>(string key, string dataKey)
        {
            return base.HashGet<T>(key, dataKey);
        }

        public new async Task<T> HashGetAsync<T>(string key, string dataKey)
        {
            return await base.HashGetAsync<T>(key, dataKey);
        }

        public new double HashIncrement(string key, string dataKey, double val = 1.0)
        {
            return base.HashIncrement(key, dataKey, val);
        }

        public new Task<double> HashIncrementAsync(string key, string dataKey, double val = 1.0)
        {
            return base.HashIncrementAsync(key, dataKey, val);
        }

        public new double HashDecrement(string key, string dataKey, double val = 1.0)
        {
            return base.HashDecrement(key, dataKey, val);
        }

        public new Task<double> HashDecrementAsync(string key, string dataKey, double val = 1.0)
        {
            return base.HashDecrementAsync(key, dataKey, val);
        }

        public new List<T> HashKeys<T>(string key)
        {
            return base.HashKeys<T>(key);
        }

        public new async Task<List<T>> HashKeysAsync<T>(string key)
        {
            return await base.HashKeysAsync<T>(key);
        }

        public new List<T> HashValues<T>(string key)
        {
            return base.HashValues<T>(key);
        }

        public new async Task<List<T>> HashValuesAsync<T>(string key)
        {
            return await base.HashValuesAsync<T>(key);
        }

        public new long ListRemove<T>(string key, T value)
        {
            return base.ListRemove(key, value);
        }

        public new Task<long> ListRemoveAsync<T>(string key, T value)
        {
            return base.ListRemoveAsync(key, value);
        }

        public new List<T> ListRange<T>(string key)
        {
            return base.ListRange<T>(key);
        }

        public new async Task<List<T>> ListRangeAsync<T>(string key)
        {
            return await base.ListRangeAsync<T>(key);
        }

        public new long ListRightPush<T>(string key, T value)
        {
            return base.ListRightPush(key, value);
        }

        public new Task<long> ListRightPushAsync<T>(string key, T value)
        {
            return base.ListRightPushAsync(key, value);
        }

        public new T ListRightPop<T>(string key)
        {
            return base.ListRightPop<T>(key);
        }

        public new async Task<T> ListRightPopAsync<T>(string key)
        {
            return await base.ListRightPopAsync<T>(key);
        }

        public new long ListLeftPush<T>(string key, T value)
        {
            return base.ListLeftPush(key, value);
        }

        public new Task<long> ListLeftPushAsync<T>(string key, T value)
        {
            return base.ListLeftPushAsync(key, value);
        }

        public new T ListLeftPop<T>(string key)
        {
            return base.ListLeftPop<T>(key);
        }

        public new async Task<T> ListLeftPopAsync<T>(string key)
        {
            return await base.ListLeftPopAsync<T>(key);
        }

        public new long ListLength(string key)
        {
            return base.ListLength(key);
        }

        public new Task<long> ListLengthAsync(string key)
        {
            return base.ListLengthAsync(key);
        }

        public new bool SortedSetAdd<T>(string key, T value, double score)
        {
            return base.SortedSetAdd<T>(key, value, score);
        }

        public new Task<bool> SortedSetAddAsync<T>(string key, T value, double score)
        {
            return base.SortedSetAddAsync(key, value, score);
        }

        public new bool SortedSetRemove<T>(string key, T value)
        {
            return base.SortedSetRemove(key, value);
        }

        public new Task<bool> SortedSetRemoveAsync<T>(string key, T value)
        {
            return base.SortedSetRemoveAsync(key, value);
        }

        public new List<T> SortedSetRangeByRank<T>(string key)
        {
            return base.SortedSetRangeByRank<T>(key);
        }

        public new async Task<List<T>> SortedSetRangeByRankAsync<T>(string key)
        {
            return await base.SortedSetRangeByRankAsync<T>(key);
        }

        public new long SortedSetLength(string key)
        {
            return base.SortedSetLength(key);
        }

        public new Task<long> SortedSetLengthAsync(string key)
        {
            return base.SortedSetLengthAsync(key);
        }

        public new bool KeyDelete(string key)
        {
            return base.KeyDelete(key);
        }

        public new Task<bool> KeyDeleteAsync(string key)
        {
            return base.KeyDeleteAsync(key);
        }

        public new long KeyDelete(List<string> keys)
        {
            return base.KeyDelete(keys);
        }

        public new Task<long> KeyDeleteAsync(List<string> keys)
        {
            return base.KeyDeleteAsync(keys);
        }

        public new bool KeyExists(string key)
        {
            return base.KeyExists(key);
        }

        public new Task<bool> KeyExistsAsync(string key)
        {
            return base.KeyExistsAsync(key);
        }

        public new bool KeyRename(string key, string newKey)
        {
            return base.KeyRename(key, newKey);
        }

        public new Task<bool> KeyRenameAsync(string key, string newKey)
        {
            return base.KeyRenameAsync(key, newKey);
        }

        public new bool KeyExpire(string key, TimeSpan? expiry = null)
        {
            return base.KeyExpire(key, expiry);
        }

        public new Task<bool> KeyExpireAsync(string key, TimeSpan? expiry = null)
        {
            return base.KeyExpireAsync(key, expiry);
        }



    }
}
