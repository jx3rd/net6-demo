﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enums
{
    public class ColumnEnums
    {
        /// <summary>
        /// 数据表详细信息字段名称
        /// </summary>
        public struct ColumnInfo
        {
            //排序码
            public const string Sort = "SortKey";

            //表名
            public const string Name = "ColumnName";

            //是否主键
            public const string Key = "IsKey";

            //占用字节数
            public const string Size = "Size";

            //是否为空
            public const string IsNull = "IsNull";

            //是否主键
            public const string CnName = "ColumnCnName";

        }
    }
}
