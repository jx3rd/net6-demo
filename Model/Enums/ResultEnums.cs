﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enums
{
    public class ResultEnums
    {

        /// <summary>
        /// 记录登录用户的变量
        /// </summary>
        public struct LOGIN
        {
            //用户编码
            public const string RYID = "ryid";

            //用户姓名
            public const string RYXM = "ryxm";

            //手机号码
            public const string PHONE = "phone";

            //主单位编码
            public const string OrgCode = "dwdm";

            //主单位名称
            public const string OrgName = "dwmc";

            //部门编码
            public const string DeptCode = "bmbm";

            //部门名称
            public const string DeptName = "bmmc";

            //TOKEN
            public const string TOKEN = "token";

            //站点ID
            public const string SITEID = "siteid";
        }

        /// <summary>
        /// 参数常量
        /// </summary>
        public struct PARAS
        {
            /// <summary>
            ///  表单数据
            /// </summary>
            public const string Form_Data = "FormData";

            /// <summary>
            ///  上一级主键
            /// </summary>
            public const string Form_PKey = "PKey";

            /// <summary>
            ///  主键
            /// </summary>
            public const string Form_Key = "Key";

            /// <summary>
            ///  编码
            /// </summary>
            public const string Form_Code = "Code";

            /// <summary>
            ///  站点ID
            /// </summary>
            public const string Form_SiteID = "SiteID";

            /// <summary>
            /// 用户编码
            /// </summary>
            public const string Form_UserCode = "UserCode";

            /// <summary>
            /// 用户名称
            /// </summary>
            public const string Form_UserName = "UserName";


            /// <summary>
            /// 单位编码
            /// </summary>
            public const string Form_OrgCode = "OrgCode";

            /// <summary>
            /// 单位名称
            /// </summary>
            public const string Form_OrgName = "OrgName";

            /// <summary>
            /// 部门编码
            /// </summary>
            public const string Form_DeptCode = "DeptCode";

            /// <summary>
            /// 部门名称
            /// </summary>
            public const string Form_DeptName = "DeptName";

            /// <summary>
            /// 是否主库
            /// </summary>
            public const string Form_IsMain = "IsMain";

            /// <summary>
            /// 下一步环节数据
            /// </summary>
            public const string Flow_NextStep = "NextStep";

            /// <summary>
            /// 退回环节数据
            /// </summary>
            public const string Flow_BackStep = "BackStep";

            /// <summary>
            /// 是否办结
            /// </summary>
            public const string Flow_IsOver = "IsOver";

            /// <summary>
            /// 表单名称
            /// </summary>
            public const string Flow_TableName = "TableName";

            /// <summary>
            /// 意见
            /// </summary>
            public const string Flow_Opinion = "Opinion";

            /// <summary>
            /// 签名路径
            /// </summary>
            public const string Flow_SignatureUrl = "SignatureUrl";

            /// <summary>
            /// 指定处理时间
            /// </summary>
            public const string Flow_AppointTime = "AppointTime";

            /// <summary>
            /// 流程待办ID
            /// </summary>
            public const string Flow_StepHandlerID = "StepHandlerID";

            /// <summary>
            /// 流程待办步骤
            /// </summary>
            public const string Flow_RunStepID = "RunStepID";

            /// <summary>
            /// 流程实例ID
            /// </summary>
            public const string Flow_RunID = "RunID";

            /// <summary>
            /// 阅办人员
            /// </summary>
            public const string Flow_Reader = "Reader";

            /// <summary>
            /// 评价星级
            /// </summary>
            public const string Flow_Star = "Star";

            /// <summary>
            ///  记录主键
            /// </summary>
            public const string Flow_RecordID = "RecordID";

            /// <summary>
            /// 批量数据
            /// </summary>
            public const string Flow_List = "list";

            /// <summary>
            /// 类型
            /// </summary>
            public const string Flow_Type = "Type";

            /// <summary>
            /// 类型
            /// </summary>
            public const string Flow_IsShowOver = "IsShowOver";

            /// <summary>
            /// 操作类型
            /// </summary>
            public const string Flow_RecordState = "Record_State";
        }

        /// <summary>
        /// 删除标记
        /// </summary>
        public struct DelFlag
        {
            //定义存放删除标记变量的参数——有效
            public const int DELFLAG_USE = 1;

            //定义存放删除标记变量的参数——无效
            public const int DELFLAG_UNUSER = -1;

        }

        /// <summary>
        /// 流程操作记录
        /// </summary>
        public struct FlowOperate
        {
            //填写
            public const string Write = "Write";

            //退回
            public const string Return = "Return";

            //拒绝
            public const string Reject = "Reject";

            //通过
            public const string Pass = "Pass";

            //办结
            public const string Finish = "Finish";

            //终止
            public const string Stop = "Stop";
        }

        /// <summary>
        /// 流程状态
        /// </summary>
        public struct FlowStatus
        {
            //退回修改
            public const string Back = "09";
        }

        /// <summary>
        /// 人员类型
        /// </summary>
        public struct UserType
        {
            /// <summary>
            /// 在册职工
            /// </summary>
            public const string ZC = "1";
            /// <summary>
            /// 聘用职工
            /// </summary>
            public const string PY = "2";
        }

        /// <summary>
        /// 流程按钮
        /// </summary>
        public struct FlowBtn
        {
            /// <summary>
            /// 办结
            /// </summary>
            public const string Over = "300a27b7-2f92-4c85-a2b3-736140b841e5";


            /// <summary>
            /// 办结
            /// </summary>
            public const string Submit = "A797DABC-5ECE-41AD-9E71-012FF5DAD3AC";


            /// <summary>
            /// 退回
            /// </summary>
            public const string Back = "D59ECB8A-C2CC-49E4-9DBC-61E9D5E9F238";


            /// <summary>
            /// 终止申报
            /// </summary>
            public const string Stop = "beaf66f6-88eb-4277-ac35-a83deeb391c3";

            /// <summary>
            /// 查看进度
            /// </summary>
            public const string Process = "2dee6660-2b92-47a4-a7ae-7aefc884c898";

            /// <summary>
            /// 关闭
            /// </summary>
            public const string Close = "e6e134b7-9a66-4952-aba8-e55f0f977d07";
        }

        /// <summary>
        /// 是否主单位
        /// </summary>
        public struct IsMainOrg
        {
            /// <summary>
            /// 是
            /// </summary>
            public const string Y = "1";
            /// <summary>
            /// 否
            /// </summary>
            public const string N = "0";
        }

        public struct OrgTypeCode
        {
            public const string ProductionUnit = "0011000003";
        }


        /// <summary>
        /// SQL查询条件符号
        /// </summary>
        public struct Symbols
        {
            /// <summary>
            /// like '%张三%'
            /// </summary>
            public const string FL = "FL";
            /// <summary>
            /// like '%张三'
            /// </summary>
            public const string LL = "LL";
            /// <summary>
            /// like '张三%'
            /// </summary>
            public const string RL = "RL";
            /// <summary>
            /// not like '%张三%'
            /// </summary>
            public const string NL = "NL";
            /// <summary>
            /// = '张三'
            /// </summary>
            public const string EQ = "EQ";
            /// <summary>
            /// in (select ..)
            /// </summary>
            public const string IN = "IN";

            /// <summary>
            /// 大于
            /// </summary>
            public const string GT = "GT";

            /// <summary>
            /// 小于
            /// </summary>
            public const string LT = "LT";

            /// <summary>
            /// 大于等于
            /// </summary>
            public const string GTEQ = "GTEQ";

            /// <summary>
            /// 小于等于
            /// </summary>
            public const string LTEQ = "LTEQ";

            /// <summary>
            /// 包含
            /// </summary>
            public const string CHARINDEX = "CHARINDEX";

            /// <summary>
            /// 在字符串中
            /// </summary>
            public const string INSTRINGS = "INSTRINGS";
        }

        /// <summary>
        /// 数据字典标识
        /// </summary>
        public struct DicTypeCode
        {
            //定义存放删除标记变量的参数——有效
            public const string DIC_CDispalyType = "System_ListColumn_CDisplayType";
        }

        /// <summary>
        /// 数据字典主键
        /// </summary>
        public struct DicTypeID
        {
            public static string DIC_InterFaceUrl = "0e4007b3-7fc6-4d25-b605-ec868d97b300";
        }

        /// <summary>
        /// 附件下载、预览
        /// </summary>
        public struct AttachOpType
        {
            public const string SINGLEDOWN = "单文件下载";
            public const string MUTILDOWN = "批量下载";
            public const string VIEW = "附件预览";
        }

        /// <summary>
        /// 权限标识
        /// </summary>
        public struct RightType
        {
            //浏览，菜单权限
            public const string Browser = "448F6DF3-1FD1-41F3-85ED-9BF1D895D5E5";
        }


        /// <summary>
        /// 数据库配置
        /// </summary>
        public struct DBPASSKEY
        {
            //平台数据
            public const string KEY = "tsy@scsjpt";

            //平台数据
            public const string COMMONPDW = "pt1qtsy";
        }

        /// <summary>
        /// 数据库
        /// </summary>
        public struct SiteDB
        {
            //平台数据
            public const string PlatFormDB = "TSY.PlatForm";
        }

        /// <summary>
        /// 菜单编码
        /// </summary>
        public struct MenuID
        {
            //平台数据
            public const string MenuID_SITEMAG = "02690902-b9ac-4fbe-a4e1-a5add98aabdb";
        }



        /// <summary>
        /// 站点
        /// </summary>
        public struct SITEWEB
        {
            public const string WebSiteID = "SiteID";

            public const string WebTestSet = "TestSet";

            public const string WebIsCache = "IsCache";
        }


        /// <summary>
        /// 流程按钮
        /// </summary>
        public struct FLOWBUTTON
        {
            //提交按钮
            public const string SUBMIT = "A797DABC-5ECE-41AD-9E71-012FF5DAD3AC";

            //提交按钮
            public const string BACK = "D59ECB8A-C2CC-49E4-9DBC-61E9D5E9F238";
        }
        /// <summary>
        /// 站点
        /// </summary>
        public struct SITEID
        {
            public const string PlatForm = "8eaaea66-37bd-4a91-a83f-9a1f028a9a1a";

        }

        /// <summary>
        /// 流程整体状态
        /// </summary>
        public struct FlowInfoStatus
        {
            //保存
            public const string Save = "0";  //保存

            public const string Check = "1";  //审核

            public const string Over = "2";  //办结

            public const string Back = "8";  //被退回

            public const string Stop = "9";  //终止

            public const string Lock = "10";  //锁定
        }



        /// <summary>
        /// 平台api
        /// </summary>
        public struct PlatFormAPI
        {
            public const string SSOCheckLoginURL = "/api/Login/SSOCheckLogin";  //单点登录api
            public const string SSOCheckTokenIDUrl = "/api/Login/SSOCheckLoginByToken";  //单点认证Tokenapi
            public const string SendWeCharMessageUrl = "/api/Message/SendWeCharMessage";  //企业消息推送
        }

        /// <summary>
        /// 接口类型
        /// </summary>
        public struct InterFaceType
        {
            public static string Interface_SCGL = "Interface_SCGL";
        }

    }
}
