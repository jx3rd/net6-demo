﻿using SqlSugar;
namespace Model.Dto.System
{
    public class SysUsersDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public int? Sex { get; set; }
        public int? Age { get; set; }
        public string NativePlace { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public DateTime? LastLogin { get; set; }

        public string LastLoginIp { get; set; }
        public int? Sort { get; set; }
        public int? IsEnable { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
