﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dto.System
{
    public class UsersLogDto
    {
        public long UserId { get; set; }
        /// <summary>
        ///  昵称
        ///</summary>
        public string NickName { get; set; }
        /// <summary>
        ///  手机号
        ///</summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        ///  用户类型
        ///</summary>
        public int? UserSysType { get; set; }
        /// <summary>
        ///  注册终端
        ///</summary>
        public int? CreatedTerminal { get; set; }
        /// <summary>
        ///  创建时间
        ///</summary>
        public DateTime? DateCreated { get; set; }
        /// <summary>
        ///  最后登录时间
        ///</summary>
        public DateTime? LastLoginTime { get; set; }
        /// <summary>
        ///  用户邮箱
        ///</summary>
        public string? Email { get; set; }

        /// <summary>
        ///  用户操作
        ///</summary>
        public string? Title { get; set; }

        /// <summary>
        ///  ip地址
        ///</summary>
        public string? IP { get; set; }

        public string? UserUrl { get; set; }
    }
}
