﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Const
{
    public class MqExChangeName
    {
        /// <summary>
        /// 个人中心任务交换机名称
        /// </summary>
        public const string Task = "Exchange_Task";

        /// <summary>
        /// 历史记录交换机名称
        /// </summary>
        public const string Browsing = "Exchange_Browsing";

        /// <summary>
        /// 消息通知交换机名称
        /// </summary>
        public const string Message = "Exchange_Message";

        /// <summary>
        /// el文章推送交换机名称
        /// </summary>
        public const string Search = "Exchange_Search";

    }
}
