﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Const
{
    public class TaskActionName
    {
        /// <summary>
        /// 登录
        /// </summary>
        public const string Login = "Login";
        /// <summary>
        /// 上传头像
        /// </summary>
        public const string UploadHeadIcon = "UploadHeadIcon";
        /// <summary>
        /// 完善用户资料
        /// </summary>
        public const string DonePersonData = "DonePersonData";
        /// <summary>
        /// 收藏文章
        /// </summary>
        public const string CollectArticle = "CollectArticle";
        /// <summary>
        /// 回答问题
        /// </summary>
        public const string AnswerTheQuestion = "AnswerTheQuestion";
        /// <summary>
        /// 使用搜索功能
        /// </summary>
        public const string UseSearchModule = "UseSearchModule";
        /// <summary>
        /// 签到
        /// </summary>
        public const string SignIn = "SignIn";
        /// <summary>
        /// 浏览
        /// </summary>
        public const string BrowseArticleOrQA = "BrowseArticleOrQA";
        /// <summary>
        /// 点赞
        /// </summary>
        public const string SupportACA = "SupportACA";
        /// <summary>
        /// 发表评论
        /// </summary>
        public const string ComeOutComment = "ComeOutComment";
    }
}
