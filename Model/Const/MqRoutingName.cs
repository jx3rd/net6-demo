﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Const
{
    public class MqRoutingName
    {
        /// <summary>
        /// 个人中心任务路由名称
        /// </summary>
        public const string Task = "Routing_Task";

        /// <summary>
        /// 系统消息路由名称
        /// </summary>
        public const string SystemMsg = "Routing_SystemMsg";

        /// <summary>
        /// el文章推送路由名称
        /// </summary>
        public const string Search = "Routing_Search";
    }
}
