﻿using SqlSugar;
namespace Model.BaseModel.System
{
    /// <summary>
    /// 
    ///</summary>
    [SugarTable("Sys_Users")]
    public class SysUsers
    {
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "ID", IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "UserName")]
        public string UserName { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Sex")]
        public int? Sex { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Age")]
        public int? Age { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "NativePlace")]
        public string NativePlace { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Address")]
        public string Address { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Password")]
        public string Password { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "LastLogin")]
        public DateTime? LastLogin { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "LastLoginIp")]
        public string LastLoginIp { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Sort")]
        public int? Sort { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "IsEnable")]
        public int? IsEnable { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "AddTime")]
        public DateTime? AddTime { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "IsDelete")]
        public int? IsDelete { get; set; }
    }
}
