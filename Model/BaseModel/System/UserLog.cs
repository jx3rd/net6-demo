﻿using Org.BouncyCastle.Crypto;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Model.BaseModel.System
{
    /// <summary>
    /// 
    ///</summary>
    [SugarTable("UserLog")]
    public class UserLog
    {
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "LogID", IsPrimaryKey = true, IsIdentity = true)]
        public long LogID { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Category")]
        public int? Category { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "CategoryTitle")]
        public string CategoryTitle { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Title")]
        public string Title { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "City")]
        public string City { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "DateCreate")]
        public DateTime? DateCreate { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Contents")]
        public string Contents { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Description")]
        public string Description { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "IP")]
        public string Ip { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "IPCity")]
        public string IPCity { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "UserID")]
        public long? UserID { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "UserName")]
        public string UserName { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "UserUrl")]
        public string UserUrl { get; set; }
        /// <summary>
        /// 终端类型 
        /// 默认值: ((0))
        ///</summary>
        [SugarColumn(ColumnName = "Terminal")]
        public int? Terminal { get; set; }
        /// <summary>
        /// 手机号码 
        ///</summary>
        [SugarColumn(ColumnName = "PhoneNumber", IsIgnore = true)]
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 邮箱地址 
        ///</summary>
        [SugarColumn(ColumnName = "Email")]
        public string Email { get; set; }
    }
}
