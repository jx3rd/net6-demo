﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Model.BaseModel.System
{
    /// <summary>
    /// 
    ///</summary>
    [SugarTable("Users")]
    public class Users
    {
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "ID", IsPrimaryKey = true, IsIdentity = true)]
        public long Id { get; set; }
        /// <summary>
        /// 登录名 
        ///</summary>
        [SugarColumn(ColumnName = "UserName")]
        public string UserName { get; set; }
        /// <summary>
        /// 密码 
        ///</summary>
        [SugarColumn(ColumnName = "Password")]
        public string Password { get; set; }
        /// <summary>
        /// 邮箱地址 
        ///</summary>
        [SugarColumn(ColumnName = "Email")]
        public string Email { get; set; }
        /// <summary>
        /// 第一次修改登录名 
        ///</summary>
        [SugarColumn(ColumnName = "FirstUserName")]
        public string FirstUserName { get; set; }
        /// <summary>
        /// 昵称 
        ///</summary>
        [SugarColumn(ColumnName = "NickName")]
        public string NickName { get; set; }
        /// <summary>
        /// 用户名称 
        ///</summary>
        [SugarColumn(ColumnName = "UserType")]
        public int? UserType { get; set; }
        /// <summary>
        /// 问题 
        ///</summary>
        [SugarColumn(ColumnName = "Question")]
        public string Question { get; set; }
        /// <summary>
        /// 问题答案 
        ///</summary>
        [SugarColumn(ColumnName = "Answer")]
        public string Answer { get; set; }
        /// <summary>
        /// 性别 
        ///</summary>
        [SugarColumn(ColumnName = "Gender")]
        public int? Gender { get; set; }
        /// <summary>
        /// 生日 
        ///</summary>
        [SugarColumn(ColumnName = "Birthday")]
        public string Birthday { get; set; }
        /// <summary>
        /// 用户余额 
        ///</summary>
        [SugarColumn(ColumnName = "UserMoney")]
        public decimal? UserMoney { get; set; }
        /// <summary>
        /// 用户积分 
        ///</summary>
        [SugarColumn(ColumnName = "UserPoints")]
        public int? UserPoints { get; set; }
        /// <summary>
        /// 行业ID 
        ///</summary>
        [SugarColumn(ColumnName = "IndustryID")]
        public int? IndustryID { get; set; }
        /// <summary>
        /// 签到天数 
        ///</summary>
        [SugarColumn(ColumnName = "RankPoints")]
        public int? RankPoints { get; set; }
        /// <summary>
        /// 信用额度 
        ///</summary>
        [SugarColumn(ColumnName = "CreditLine")]
        public decimal? CreditLine { get; set; }
        /// <summary>
        /// 建立时间 
        ///</summary>
        [SugarColumn(ColumnName = "DateCreated")]
        public DateTime? DateCreated { get; set; }
        /// <summary>
        /// 状态1正常，0锁定 
        ///</summary>
        [SugarColumn(ColumnName = "Status")]
        public int? Status { get; set; }
        /// <summary>
        /// 登录次数 
        ///</summary>
        [SugarColumn(ColumnName = "LoginCount")]
        public int? LoginCount { get; set; }
        /// <summary>
        /// 最后登录时间 
        ///</summary>
        [SugarColumn(ColumnName = "LastLoginTime")]
        public DateTime? LastLoginTime { get; set; }
        /// <summary>
        /// 最后登陆IP 
        ///</summary>
        [SugarColumn(ColumnName = "LastLoginIP")]
        public string LastLoginIP { get; set; }
        /// <summary>
        /// 是否删除 
        /// 默认值: ((0))
        ///</summary>
        [SugarColumn(ColumnName = "IsDelete")]
        public bool IsDelete { get; set; }
        /// <summary>
        /// 用户经验值 
        ///</summary>
        [SugarColumn(ColumnName = "UserEmpiricalValue")]
        public int? UserEmpiricalValue { get; set; }
        /// <summary>
        /// 用是否补足积分 
        /// 默认值: ('0')
        ///</summary>
        [SugarColumn(ColumnName = "IsReplenish")]
        public bool? IsReplenish { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Upgrade")]
        public int? Upgrade { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "IdentityCheckCode")]
        public string IdentityCheckCode { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "WeChatOpenID")]
        public string WeChatOpenID { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "WeChatUnionID")]
        public string WeChatUnionID { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "WeChatPublicOpenID")]
        public string WeChatPublicOpenID { get; set; }
        /// <summary>
        /// 用户类型
        /// 默认值: ((0))
        ///</summary>
        [SugarColumn(ColumnName = "UserSysType")]
        public int? UserSysType { get; set; }
        /// <summary>
        /// 手机号码 
        ///</summary>
        [SugarColumn(ColumnName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        /// <summary>
        ///  
        /// 默认值: ((0))
        ///</summary>
        [SugarColumn(ColumnName = "CreatedTerminal")]
        public int CreatedTerminal { get; set; }
        /// <summary>
        /// 个人简介 
        ///</summary>
        [SugarColumn(ColumnName = "Biography")]
        public string Biography { get; set; }
        /// <summary>
        /// 个人头像 
        ///</summary>
        [SugarColumn(ColumnName = "HeadImgUrl")]
        public string HeadImgUrl { get; set; }
    }
}
