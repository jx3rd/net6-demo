﻿using FluentValidation;
using Model.Dto.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ValidationModel.System
{
    public class SysUsersValidator : AbstractValidator<SysUsersDto>
    {
        public SysUsersValidator()
        {
            RuleFor(x => x.Id).NotNull();
            //RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Age).InclusiveBetween(18, 60);
        }
    }
}
