﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ApiModel.ApiResult
{
    /// <summary>
    /// API返回结果泛型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseResult<T>
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool success { get; set; } = true;

        /// <summary>
        /// 消息描述
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 返回结果
        /// </summary>
        public T data { get; set; }

        /// <summary>
        /// 失败返回结果
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        public static ResponseResult<T> FailResult(string message, T data)
        {
            return new ResponseResult<T> { success = false, message = message, data = data };
        }
    }

    /// <summary>
    /// API返回结果非泛型
    /// </summary>
    public class ResponseResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool success { get; set; } = true;

        /// <summary>
        /// 消息描述
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 返回结果
        /// </summary>
        public object data { get; set; }

        /// <summary>
        /// 成功返回结果
        /// </summary>
        /// <param name="message">消息</param>
        /// <returns></returns>
        public static ResponseResult SuccessResult(string message)
        {
            return new ResponseResult { success = true, message = message };
        }

        /// <summary>
        /// 返回结果
        /// </summary>
        /// <param name="success"></param>
        /// <param name="data"></param>
        /// <param name="message">消息</param>
        /// <returns></returns>
        public static ResponseResult Result(bool success, object data, string message)
        {
            return new ResponseResult { success = success, data = data, message = message };
        }

        /// <summary>
        /// 失败返回结果
        /// </summary>
        /// <param name="message">消息</param>
        /// <returns></returns>
        public static ResponseResult FailResult(string message)
        {
            return new ResponseResult { success = false, message = message };
        }
    }
}
