﻿namespace Model.ApiModel.ApiGroup
{
    public enum ApiGroupNames
    {

        [GroupInfo(Title = "系统管理", Description = "系统管理相关接口", Version = "v1")]
        System = 1,
        [GroupInfo(Title = "用户管理", Description = "用户管理相关接口", Version = "v1")]
        User = 2,
        [GroupInfo(Title = "文章管理", Description = "文章管理相关接口", Version = "v1")]
        Article = 3,
        [GroupInfo(Title = "广告管理", Description = "广告管理相关接口", Version = "v1")]
        Advertisement = 4,
        [GroupInfo(Title = "社区管理", Description = "社区管理相关接口", Version = "v1")]
        Community = 5,
        [GroupInfo(Title = "消息管理", Description = "系统消息相关接口", Version = "v1")]
        Message = 6,
        [GroupInfo(Title = "商品管理", Description = "商品兑换相关接口", Version = "v1")]
        Goods = 7,
        [GroupInfo(Title = "公共", Description = "公共工具类相关接口", Version = "v1")]
        Common = 8
    }
}
